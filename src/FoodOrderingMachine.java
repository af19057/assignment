import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JButton niboshiButton;
    private JButton misoButton;
    private JButton shioButton;
    private JButton gyozaButton;
    private JButton friedRiceButton;
    private JButton tsukemenButton;
    private JTextPane orderedItemslist;
    private JButton checkOutButton;
    private JLabel totalPrice;
    private int niboshiPrice=850;
    private int misoPrice=800;
    private int shioPrice=750;
    private int gyozaPrice=400;
    private int friedRicePrice=650;
    private int tsukemenPrice=700;
    private int total=0;
    private String selectSizes[]={"Regular +0yen","Large +100yen","Extra Large +150yen"};

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int foodPrice){
        if(food != "Gyoza"){
            int size=JOptionPane.showOptionDialog(null,
                    "Choose the size of "+food+".",
                    "Size Choice",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    selectSizes,
                    selectSizes[0]
            );
            if(size == 0){
                food += " (R)size";
            }
            else if(size == 1){
                food += " (L)size";
                foodPrice += 100;
            }
            else if(size == 2){
                food += " (EL)size";
                foodPrice += 150;
            }
            else {
                return;
            }
        }

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering "+food+"! It will be received as soon as possible.");
            String currentText = orderedItemslist.getText();
            orderedItemslist.setText(currentText+food+" "+foodPrice+"yen"+"\n");
            total += foodPrice;
            totalPrice.setText("Total     "+total+" yen");
        }
    }

    public FoodOrderingMachine() {
        niboshiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Niboshi Ramen",niboshiPrice);
            }
        });

        niboshiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Niboshi_ramen.jpg")
        ));

        misoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Miso Ramen",misoPrice);
            }
        });

        misoButton.setIcon(new ImageIcon(
                this.getClass().getResource("Miso_ramen.jpg")
        ));

        shioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shio Ramen",shioPrice);
            }
        });

        shioButton.setIcon(new ImageIcon(
                this.getClass().getResource("Shio_ramen.jpg")
        ));


        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",gyozaPrice);
            }
        });

        gyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Gyoza.jpg")
        ));


        friedRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried Rice",friedRicePrice);
            }
        });

        friedRiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("Fried_rice.jpg")
        ));


        tsukemenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tsukemen",tsukemenPrice);
            }
        });

        tsukemenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tsukemen.jpg")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation=JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you! The total Price is "+total+" yen"+"\n"+"You got "+total/100+" points!");
                    total = 0;
                    totalPrice.setText("Total     "+total+" yen");
                    orderedItemslist.setText(null);
                }
            }
        });
    }
}
